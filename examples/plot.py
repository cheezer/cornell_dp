__author__ = 'michaelxie'

from pylab import rcParams
import matplotlib.pyplot as plt
import copy

class plot:
    def __init__(self, fileNames, skips):
        self.acc = []
        self.fileNames = fileNames
        self.skips = skips
        for i in range(2):
            self.acc.append([[], [], []])
            self.getData(copy.deepcopy(fileNames[i]), skips[i], self.acc[i])

    def getData(self, fileName, skip, acc):
        fi = open(fileName, "r")
        data = [[], [], []]
        #acc =
        se = -1
        now = 0
        for st in fi.readlines():
            if (st.find("epoch # 1 ") != -1):
                now += 1
            if (st.find("terminate") != -1 or st.find("epoch # 1 ") != -1) and (se != -1):
                for j in range(3):
                    acc[j].append(copy.deepcopy(data[j]))
                    data[j] = []
            if skip == 0 or now > 1:
                if st.find("accuracy") != -1:
                    ss = st[st.find("accuracy"):]
                    list = ss.split(" ")
                    se = (se + 1) % 3
                    num = list[2]
                    data[se].append(float(num))

    def plotInd(self, i):
        acc = self.acc[i]
        fileName = self.fileNames[i]
        plt.figure(i)
        for j in range(3):
            plt.subplot(3, 1, j + 1)
            plt.title(["train", "validation", "test"][j])
            plt.ylabel('accuracy')
            print(len(acc[j]))
            if len(acc[j]) == 3:
                plt.plot(range(len(acc[j][0])), acc[j][0], 'bo', label='input layer')
                plt.plot(range(len(acc[j][1])), acc[j][1], 'ro', label='first hidden layer')
                plt.plot(range(len(acc[j][2])), acc[j][2], 'go', label='second hidden layer')
                plt.legend(loc='lower right', prop={'size':7})
        plt.xlabel("# of iteration")
        plt.savefig(fileName.replace(".txt", '.png'), bbox_inches='tight')
        plt.show()

    def plotJoint(self):
        rcParams['figure.figsize'] = 20, 15
        plt.figure(0)
        for i in range(3): #train, validation, test
            for j in range(3): #layers
                plt.subplot(3, 3, (j + 1) + i * 3)
                plt.title(["train", "validation", "test"][i] + " - " + ['input layer', 'first hidden layer', 'second hidden layer'][j])
                plt.ylabel('accuracy')
                plt.plot(range(len(self.acc[0][i][j])), self.acc[0][i][j], 'bo', label="Trained") #train
                plt.plot(range(len(self.acc[1][i][j])), self.acc[1][i][j], 'ro', label="Random") #random
                plt.legend(loc='lower right', prop={'size':7})
                #plt.xlabel("# of iteration")
        plt.savefig("compare.png", bbox_inches='tight')
        plt.show()

p = plot(["trainedOutput16.txt", "randomOutput16.txt"], [1, 0])
p.plotJoint()

