require 'dp'
--require '/home/michaelxie/workspace/dp/data/halfCifar10'
package.path = package.path .. ";../data/?.lua"

require 'halfCifar10'

--[[command line arguments]]--

cmd = torch.CmdLine()
cmd:text()
cmd:text('Image Classification using MLP Training/Optimization')
cmd:text('Example:')
cmd:text('$> th neuralnetwork.lua --batchSize 128 --momentum 0.5')
cmd:text('Options:')
cmd:option('--learningRate', 0.1, 'learning rate at t=0')
cmd:option('--schedule', '{[200]=0.01, [256]=0.01, [400]=0.001, [16]=0.03, [512]=0.0003}', 'learning rate schedule')
cmd:option('--maxOutNorm', 1, 'max norm each layers output neuron weights')
cmd:option('--momentum', 0, 'momentum')
cmd:option('--hiddenSize', '{256,256}', 'number of hidden units per layer')
cmd:option('--batchSize', 32, 'number of examples per batch')
cmd:option('--cuda', false, 'use CUDA')
cmd:option('--useDevice', 1, 'sets the device (GPU) to use')
cmd:option('--maxEpoch', 100, 'maximum number of epochs to run')
cmd:option('--maxTries', 30, 'maximum number of epochs to try to find a better local minima for early-stopping')
cmd:option('--dropout', false, 'apply dropout on hidden neurons')
cmd:option('--batchNorm', false, 'use batch normalization. dropout is mostly redundant with this')
cmd:option('--dataset', 'Mnist', 'which dataset to use : Mnist | NotMnist | Cifar10 | Cifar100')
cmd:option('--standardize', false, 'apply Standardize preprocessing')
cmd:option('--zca', false, 'apply Zero-Component Analysis whitening')
cmd:option('--lecunlcn', false, 'apply Yann LeCun Local Contrast Normalization')
cmd:option('--progress', false, 'display progress bar')
cmd:option('--silent', false, 'dont print anything to stdout')
cmd:option('--half', 0, '0 full | 1 first half| 2 last half ')
cmd:option('--debugPartTwo', false)
cmd:option('--randomSeed', 0, 'integer')
cmd:text()
opt = cmd:parse(arg or {})
opt.schedule = dp.returnString(opt.schedule)
opt.hiddenSize = dp.returnString(opt.hiddenSize)
if not opt.silent then
   table.print(opt)
end

--[[preprocessing]]--

local input_preprocess = {}
if opt.standardize then
   table.insert(input_preprocess, dp.Standardize())
end
if opt.zca then
   table.insert(input_preprocess, dp.ZCA())
end
if opt.lecunlcn then
   table.insert(input_preprocess, dp.GCN())
   table.insert(input_preprocess, dp.LeCunLCN{progress=true})
end

if (not opt.debugPartTwo) then 
    --[[data]]--
    if opt.dataset == 'Mnist' then
       ds = dp.Mnist{input_preprocess = input_preprocess}
    elseif opt.dataset == 'NotMnist' then
       ds = dp.NotMnist{input_preprocess = input_preprocess}
    elseif opt.dataset == 'Cifar10' then
        if opt.half > 0 then
            Cifar10:__init({input_preprocess = input_preprocess}, opt.half)
            ds = Cifar10
        else
            ds = Cifar10{input_preprocess = input_preprocess}
        end
    elseif opt.dataset == 'Cifar100' then
       ds = dp.Cifar100{input_preprocess = input_preprocess}
    else
        error("Unknown Dataset")
    end
    --[[Model]]--
    model = nn.Sequential()
    model:add(nn.Convert(ds:ioShapes(), 'bf')) -- to batchSize x nFeature (also type converts)

    -- hidden layers
    inputSize = ds:featureSize()
    for i,hiddenSize in ipairs(opt.hiddenSize) do
       model:add(nn.Linear(inputSize, hiddenSize)) -- parameters
       if opt.batchNorm then
          model:add(nn.BatchNormalization(hiddenSize))
       end
       model:add(nn.Tanh())
       if opt.dropout then
          model:add(nn.Dropout())
       end
       inputSize = hiddenSize
    end

    model:add(nn.Linear(inputSize, #(ds:classes())))
    model:add(nn.LogSoftMax())

    --[[Propagators]]--

    train = dp.Optimizer{
       acc_update = opt.accUpdate,
       loss = nn.ModuleCriterion(nn.ClassNLLCriterion(), nil, nn.Convert()),
       callback = function(model, report) 
          opt.learningRate = opt.schedule[report.epoch] or opt.learningRate
          if opt.accUpdate then
             model:accUpdateGradParameters(model.dpnn_input, model.output, opt.learningRate)
          else
             model:updateGradParameters(opt.momentum) -- affects gradParams
             model:updateParameters(opt.learningRate) -- affects params
          end
          model:maxParamNorm(opt.maxOutNorm) -- affects params
          model:zeroGradParameters() -- affects gradParams 
       end,
       feedback = dp.Confusion(),
       sampler = dp.ShuffleSampler{batch_size = opt.batchSize},
       progress = opt.progress
    }
    valid = dp.Evaluator{
       feedback = dp.Confusion(),
       sampler = dp.Sampler{batch_size = opt.batchSize}
    }
    test = dp.Evaluator{
       feedback = dp.Confusion(),
       sampler = dp.Sampler{batch_size = opt.batchSize}
    }

    --[[Experiment]]--

    xp = dp.Experiment{
       model = model,
       optimizer = train,
       validator = valid,
       tester = test,
       observer = {
          dp.FileLogger(),
          dp.EarlyStopper{
             error_report = {'validator','feedback','confusion','accuracy'},
             maximize = true,
             max_epochs = opt.maxTries
          }
       },
        random_seed = opt.random_seed,
       --random_seed = os.time(),
       max_epoch = opt.maxEpoch
    }

    --[[GPU or CPU]]--

    if opt.cuda then
       require 'cutorch'
       require 'cunn'
       cutorch.setDevice(opt.useDevice)
       xp:cuda()
    end

    xp:verbose(not opt.silent)
    if not opt.silent then
       print"Model :"
       print(model)
    end
    xp:run(ds)
else
    model = nn.Sequential()
    model:add(nn.Convert('bchw', 'bf')) -- to batchSize x nFeature (also type converts)
    -- hidden layers
    inputSize = 3072
    for i,hiddenSize in ipairs(opt.hiddenSize) do
       model:add(nn.Linear(inputSize, hiddenSize)) -- parameters
       if opt.batchNorm then
          model:add(nn.BatchNormalization(hiddenSize))
       end
       model:add(nn.Tanh())
       if opt.dropout then
          model:add(nn.Dropout())
       end
       inputSize = hiddenSize
    end
    model:add(nn.Linear(inputSize, 10))
    model:add(nn.LogSoftMax())
end
--torch.save("model.obj", model)
collectgarbage()
if opt.half > 0 then
    for i = 0, #opt.hiddenSize do
        Cifar10._feature_size = 3*32*32
        Cifar10._image_axes = 'bchw'
        if i == 0 then
            Cifar10:__init({input_preprocess = input_preprocess}, 3 - opt.half)
        else
            Cifar10:__init({input_preprocess = input_preprocess}, 3 - opt.half, i, model, opt.hiddenSize[i])
        end
        dt = Cifar10
        collectgarbage()

        newNet = nn.Sequential()
        newNet:add(nn.Convert(dt:ioShapes(), 'bf')) -- to batchSize x nFeature (also type converts)

        -- hidden layers
        inputSize = dt:featureSize()
        for i,hiddenSize in ipairs(opt.hiddenSize) do
            newNet:add(nn.Linear(inputSize, hiddenSize)) -- parameters
            if opt.batchNorm then
                newNet:add(nn.BatchNormalization(hiddenSize))
            end
            newNet:add(nn.Tanh())
            if opt.dropout then
                newNet:add(nn.Dropout())
            end
            inputSize = hiddenSize
        end

        newNet:add(nn.Linear(inputSize, #(dt:classes())))
        newNet:add(nn.LogSoftMax())

        --[[Propagators]]--

        train = dp.Optimizer{
            acc_update = opt.accUpdate,
            loss = nn.ModuleCriterion(nn.ClassNLLCriterion(), nil, nn.Convert()),
            callback = function(newNet, report)
                opt.learningRate = opt.schedule[report.epoch] or opt.learningRate
                if opt.accUpdate then
                    newNet:accUpdateGradParameters(newNet.dpnn_input, newNet.output, opt.learningRate)
                else
                    newNet:updateGradParameters(opt.momentum) -- affects gradParams
                    newNet:updateParameters(opt.learningRate) -- affects params
                end
                newNet:maxParamNorm(opt.maxOutNorm) -- affects params
                newNet:zeroGradParameters() -- affects gradParams
            end,
            feedback = dp.Confusion(),
            sampler = dp.ShuffleSampler{batch_size = opt.batchSize},
            progress = opt.progress
        }
        valid = dp.Evaluator{
            feedback = dp.Confusion(),
            sampler = dp.Sampler{batch_size = opt.batchSize}
        }
        test = dp.Evaluator{
            feedback = dp.Confusion(),
            sampler = dp.Sampler{batch_size = opt.batchSize}
        }

        --[[Experiment]]--

        xp = dp.Experiment{
            model = newNet,
            optimizer = train,
            validator = valid,
            tester = test,
            observer = {
                dp.FileLogger(),
                dp.EarlyStopper{
                    error_report = {'validator','feedback','confusion','accuracy'},
                    maximize = true,
                    max_epochs = opt.maxTries
                }
            },
            random_seed = opt.random_seed,
            max_epoch = opt.maxEpoch
        }

        --[[GPU or CPU]]--
        if opt.cuda then
            require 'cutorch'
            require 'cunn'
            cutorch.setDevice(opt.useDevice)
            xp:cuda()
        end

        xp:verbose(not opt.silent)
        if not opt.silent then
            print("newNet :", i)
            print(newNet)
        end
        xp:run(dt)
    end
end


print "terminate!"
